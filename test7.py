from selenium import webdriver
from selenium.webdriver.common.by import By
import math
import time


link =  'http://suninjuly.github.io/execute_script.html'
browser = webdriver.Chrome()
browser.get(link)

def calc(x):
    return str(math.log(abs(12*math.sin(int(x)))))
x_element = browser.find_element(By.ID, 'input_value')
x = x_element.text
y = calc(x)

input1 = browser.find_element(By.ID, 'answer').send_keys(y)
input2 = browser.find_element(By.ID, 'robotCheckbox').click()
browser.execute_script("window.scrollBy(0, 100);")
input3 = browser.find_element(By.ID, 'robotsRule').click()
input4 = browser.find_element(By.CSS_SELECTOR, 'button.btn').click()

time.sleep(10)
browser.quit()
