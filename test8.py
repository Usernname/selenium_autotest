from selenium import webdriver
from selenium.webdriver.common.by import By
import os
import time

link =  'http://suninjuly.github.io/file_input.html'
browser = webdriver.Chrome()
browser.get(link)

browser.find_element(By.NAME, 'firstname').send_keys('Van')
browser.find_element(By.NAME, 'lastname').send_keys('Gan')
browser.find_element(By.NAME, 'email').send_keys('yaaa')

current_dir = os.path.abspath(os.path.dirname(__file__))    
file_path = os.path.join(current_dir, '1.txt')
browser.find_element(By.ID, 'file').send_keys(file_path)
browser.find_element(By.CSS_SELECTOR, 'button.btn').click()


time.sleep(5)