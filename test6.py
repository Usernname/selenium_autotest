from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select

link = 'http://suninjuly.github.io/selects1.html'
browser = webdriver.Chrome()
browser.get(link)

x = browser.find_element(By.ID, 'num1')
y = browser.find_element(By.ID, 'num2')
x = x.text
y = y.text

sum = int(x) + int(y)

select = Select(browser.find_element(By.ID, 'dropdown'))
select.select_by_value(str(sum))
browser.find_element(By.CSS_SELECTOR, 'button.btn').click()


