from selenium import webdriver
from selenium.webdriver.common.by import By
import math

link =  'http://suninjuly.github.io/get_attribute.html'
browser = webdriver.Chrome()
browser.get(link)

def calc(x):
    return str(math.log(abs(12*math.sin(int(x)))))

treasure = browser.find_element(By.ID, 'treasure')
gold = treasure.get_attribute('valuex')

x = calc(gold)

input1 = browser.find_element(By.ID, 'answer').send_keys(x)
input2 = browser.find_element(By.ID, 'robotCheckbox').click()
input3 = browser.find_element(By.ID, 'robotsRule').click()
input3 = browser.find_element(By.CSS_SELECTOR, 'button.btn').click()
